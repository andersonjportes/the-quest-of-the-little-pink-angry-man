﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saw : MonoBehaviour
{

    [SerializeField] private float _speed = 1;
    [SerializeField] private float _moveTime = 2;

    public float speed => _speed;
    public float moveTime => _moveTime;
    private bool dirRight = true;
    private float timer;

    void Update()
    {
        if(dirRight)
        {
            //SE VERDADEIRO, SERRA VAI PARA DIREITA
            transform.Translate(Vector2.right * speed * Time.deltaTime);
        }
        else
        {
            //SE FALSO, SERRA VAI PARA ESQUERDA
            transform.Translate(Vector2.left * speed * Time.deltaTime);
        }

        timer += Time.deltaTime;

        if(timer >= moveTime)
        {
            dirRight = !dirRight;
            timer = 0f;
        }
    }
}
