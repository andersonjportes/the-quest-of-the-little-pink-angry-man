using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController instance;
    Animator anime;

    void Awake()
    {
        instance = this;  
    }

    void Start()
    {
        anime = GetComponent<Animator>();
    }

    // Update is called once per frame
    public void Shake()
    {
        anime.Play("Shake");
    }
}
