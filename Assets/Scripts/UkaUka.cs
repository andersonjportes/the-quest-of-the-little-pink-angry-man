﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UkaUka : MonoBehaviour
{
    private Rigidbody2D rigBody;
    private Animator animations;
    private bool colliding;
    public float speed;
    public Transform rightCollision;
    public Transform leftCollision;
    public Transform topCollision;
    public LayerMask layer;
    public BoxCollider2D boxCollider2D;
    public CircleCollider2D circleCollider2D;
    public bool playerDestroyed;

    // Start is called before the first frame update
    void Start()
    {
        rigBody = GetComponent<Rigidbody2D>();
        animations = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        rigBody.velocity = new Vector2(speed, rigBody.velocity.y);
        colliding = Physics2D.Linecast(rightCollision.position, leftCollision.position, layer);

        if(colliding)
        {
            transform.localScale = new Vector2(transform.localScale.x * -1f, transform.localScale.y);
            speed = -speed;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            float height = collision.contacts[0].point.y - topCollision.position.y;

            if(height > 0 && !playerDestroyed)
            {
                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 10, ForceMode2D.Impulse);
                speed = 0;
                animations.SetTrigger("die");
                boxCollider2D.enabled = false;
                circleCollider2D.enabled = false;
                rigBody.bodyType = RigidbodyType2D.Kinematic;
                AudioController.current.PlayMusic(AudioController.current.enemyScream);
                Destroy(gameObject, 0.42f);
            }
            else
            {
                playerDestroyed = true;
                CameraController.instance.Shake();
                GameController.instance.ShowGameOver();
                Destroy(collision.gameObject);
            }

        }
    }
}
