﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    [SerializeField] private float _movementSpeed = 5;
    [SerializeField] private float _jumpSpeed = 3;

    public float speed => _movementSpeed;
    public float jumpForce => _jumpSpeed;
    public bool isJumping;
    public bool doubleJump;
    private Rigidbody2D rigid;
    private Animator animator;
    private InputSystem _inputSystem;
    public bool isBlowing; 

    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        _inputSystem = new InputSystem();
        _inputSystem.Enable();
        _inputSystem.Player.Pulo.performed += Pular;
    }

    void Update()
    {
        //IMPLEMENTAÇÕS INPUT ANTIGO
        Jump();
        Move();

        //IMPLEMENTAÇÕS INPUT ANTIGO
        //Vector2 movi = _inputSystem.Player.Caminhar.ReadValue<Vector2>();
        //Caminhar(movi);
    }

    void Move()
    {
        // 3 paramentros no vector 3. Eixo x, y, z
        // Como só queremos que ele ande para os lados, devemos apenas passar informaçoes para o eixo x
        // Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
        // transform.position += movement * Time.deltaTime * speed;

        float movement = Input.GetAxis("Horizontal");

        rigid.velocity = new Vector2 (movement * speed, rigid.velocity.y);

        if(movement > 0f)
        {
            animator.SetBool("walk", true);
            transform.eulerAngles = new Vector3(0f, 0f, 0f);
        }

        if(movement < 0f)
        {
            animator.SetBool("walk", true);
            transform.eulerAngles = new Vector3(0f, 180f, 0f);
        }

        if(movement == 0f)
        {
            animator.SetBool("walk", false);
        }
    }

    void Jump()
    {

        //PULO SIMPLES FUNCIONAL
        // if(Input.GetButtonDown("Jump")){
        //     rigid.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);    
        // }

        //IMPLEMENTAÇÃO COM PULO DUPLO 
        if (Input.GetButtonDown("Jump") && !isBlowing) //isJumping precisa ser falso.
        {
            if(!isJumping)
            {
                rigid.AddForce(new Vector2(0f,jumpForce), ForceMode2D.Impulse);
                doubleJump = true;
                animator.SetBool("jump", true);
            }
            else
            {
                if(doubleJump)
                {
                    rigid.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
                    animator.SetBool("doubleJump", true);
                    doubleJump = false;
                }
            }
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.layer == 8) // layer Ground
        {
            isJumping = false; //Se ele está no chao, logo nao está pulando, assim FALSE
            doubleJump = false;
            animator.SetBool("jump", false);
        }

        if(collision.gameObject.tag == "Spike" || collision.gameObject.tag == "Saw") // tag Spike e Saw
        {
            GameController.instance.ShowGameOver();
            Destroy(gameObject);
            AudioController.current.PlayMusic(AudioController.current.sfx);
            CameraController.instance.Shake();
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.gameObject.layer == 8) // layer Ground
        {
            isJumping = true;
        }  
    }

    void  OnTriggerStay2D(Collider2D collider) {
        if(collider.gameObject.layer == 11)
        {
            isBlowing = true;
        }
    }

    void OnTriggerExit2D(Collider2D collider) {
        if(collider.gameObject.layer == 11)
        {
            isBlowing = false;
        }       
    }

    //NOVA IMPLEMENTAÇÂO PULAR
    public void Pular(InputAction.CallbackContext context)
    {
        rigid.AddForce(new Vector2(0f, jumpForce * 2), ForceMode2D.Impulse);
    }


    //NOVA IMPLEMENTAÇÂO CAMINHAR
    public void Caminhar(Vector2 movi)
    {
        transform.Translate(movi.x, 0, 0);
    }

}
