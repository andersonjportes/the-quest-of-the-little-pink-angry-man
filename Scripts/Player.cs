﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Start is called before the first frame update
    
    public float forcaPulo;
    public float velocidadeMaxima;
    void Start()
    {
        GetComponent<Rigidbody2D>().AddForce(new Vector2(0,200));
    }

    // Update is called once per frame
    void Update()
    {
        float movimentoH = Input.GetAxis("Horizontal");
        GetComponent<Rigidbody2D>().velocity = new Vector2(movimentoH*velocidadeMaxima, GetComponent<Rigidbody2D>().velocity.y);

        if (movimentoH < 0)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else if (movimentoH > 0)
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
             GetComponent<Rigidbody2D>().AddForce(new Vector2(0, forcaPulo));
        }
    }
}
